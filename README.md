# Let me live in Buckinghamshire

I wrote this song while at university. The story behind it is a conversation I
had with friends about songs that celebrate particular cities or counties. I was
explaining that Adge Cutler, founder of the Wurzels, had written the song *Drink
up thy Zider* for use in Somerset pub sing-alongs since he was fed up of having
to sing *Maybe It's Because I'm a Londoner* and *I Belong to Glasgow*, being
neither a Londoner nor Glaswegian.

My friend James recalled that he'd been made to sing *Sussex by the Sea* as part
of a county-wide schools event, but another of my friends, Paul McClarty, could
not think of any similar song for Buckinghamshire. What a shame. Well, we
couldn't have that, so I wrote him one and this is it.

## Summary information

  - *Voicing:* Solo voice

  - *Notes on ambitus:* 1 octave, E flat to E flat, though brave singers might
    attempt a top A flat for the last note.

  - *Instrumentation:* Piano

  - *Approximate performance length:* 3:30

  - *Licence:* Creative Commons Attribution-NonCommercial 4.0 International
     Public Licence: <https://creativecommons.org/licenses/by-nc/4.0/>

## Files

This repository contains the [Lilypond](http://lilypond.org) source code. To
compile it yourself, you will also need my [house style
files](https://gitlab.com/alex-ball-music/ajb-lilypond).

For PDF and MIDI downloads, see the [Releases
page](https://gitlab.com/alex-ball-music/buckinghamshire/-/releases).
