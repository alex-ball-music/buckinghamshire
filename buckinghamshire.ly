\version "2.18.0"

\include "english.ly"
\include "ajb-setup.ly"
theStaffSize = #19
\include "ajb-paper.ly"
\include "ajb-layout.ly"
\include "articulate.ly"

\header {
  dedication = \markup { \italic { "For Paul McClarty" } }
  title = \markup \sans "Let me live in Buckinghamshire"
  composer = "Alex Ball"
  copyright = \markup { \override #'(baseline-skip . 3) \center-column {
    "© Alex Ball 2000. Released under a Creative Commons Attribution Non-Commercial 4.0 International"
    "Licence: http://creativecommons.org/licenses/by-nc/4.0/" } }
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
}

global = {
  \key af \major
  \time 4/4
  \tempo "Triumphantly" 4 = 120
}

control = {
  \repeat volta 3 {
    s1 * 17
    s2. \bar "||" s4
    s1 * 14
  }
  \alternative {
    { s1 * 4 }
    { s1 }
  }
  s1 \bar "|." 
}

sopranoVoice = \relative c' {
  \global
  \dynamicUp
  \repeat volta 3 {
    % intro
    R1
    r2 r4 ef
    % verse
    c'4 c c bf
    << \new Voice = "alternativeOne"
      { \voiceTwo \tiny bf4. af8 }
      { \voiceOne \normalsize bf8 af4. } >>
      \oneVoice af4 \slurDashed af8( af)
    af4 bf << \new Voice = "alternativeTwo"
      { \voiceTwo \tiny af4. f8 }
      { \voiceOne \normalsize af8 f4. } >> \oneVoice
    ef2 r4 ef4
    bf'4 bf af bf
    c4 bf bf4. af8
    g4 af g4. f8
    ef2. ef4
    c'4 c c bf
    << \new Voice = "alternativeThree"
      { \voiceTwo \tiny bf4. af8 }
      { \voiceOne \normalsize bf8 af4. } >>
      \oneVoice af4 af
    af4 bf af4. f8
    ef2 r4 ef4
    bf'4 bf8( bf) bf4 c
    df8( df) bf4 bf g8( g)
    af4 g c bf
    af2. ef4
    % chorus
    c'4 c c df
    ef8 bf c4 bf8 af4.
    af4. bf8 af f4.
    ef2 r4 ef
    bf'4 bf bf c
    df4. bf8 bf4 g
    ef4 f ef df
    c2 r4 ef4
    c'4 c c df
    ef8 c4. c af8
    bf8 af4. bf4 c
    df2 df
    c4 c c df
    ef8 bf c4 bf8 af4.
  }
  \alternative {
    {
      ef4. df'8 c bf4.
      af2. r4
      R1
      R1
    } {
      ef4. df'8 ef4 ef
    }
  }
  ef2 r
}


verseOne = \lyricmode {
  \set stanza = "1."
  Now I was born one sun -- ny morn
  \unSlur in the \reSlur shire of Buck -- ing -- ham:
  a land a -- part at Eng -- land’s heart,
  whence her -- oes oft have come.
  And as a boy it was my joy
  to go and feed the ducks.
  I was so __ glad my Mum __ and Dad
  had __ brought me up in Bucks.
}

chorus = \lyricmode {
  Oh let me live in Buck -- ing -- ham -- shi -- re,
  let me lie in Bucks!
  With -- out its calm -- ing in -- flu -- ence
  my life would be in flux.
  I tried to find a bet -- ter place,
  I did -- n’t have much luck
  so let me live in Buck -- ing -- ham -- shi -- re,
}

chorusLastLineOne = \lyricmode {
  let me lie in Bucks!
}

chorusLastLineTwo = \lyricmode {
  let me die in Bucks!
}

verseTwo = \lyricmode {
  \set stanza = "2."
  Just leave me roam
    \set associatedVoice = alternativeOne a -- bout
    \unset associatedVoice my home
  \unSlur where the \reSlur dirt
    \set associatedVoice = alternativeTwo is fresh
    \unset associatedVoice and clean.         
  The sky is blue, the coun -- cil too,
  the ri -- vers e -- ver -- green.
  There Young and Old
    \set associatedVoice = alternativeThree Con -- ser --
    \unset associatedVoice va -- tives
  can live in har -- mo -- ny,
  so let \unSlur me re -- \reSlur tire to \unSlur Buck -- ing -- \reSlur ham -- shire
  \unSlur it’s the \reSlur on -- ly place for me!
}

verseThree = \lyricmode {
  \set stanza = "3."
  I long to lie 
    \set associatedVoice = alternativeOne be -- neath
    \unset associatedVoice the sky
  I __ knew
    \set associatedVoice = alternativeTwo when I
    \unset associatedVoice was young.
  I’d sniff the fair and bal -- my air,
  the fra -- grant smell of dung.
  From High Wy -- combe to Che -- sham,
  Mil -- ton Keynes to Ayles -- bu -- ry,
  I love my __ coun -- ty an __ a -- mount
  ye’ll __ ne -- ver else -- where see.
}

right = \relative c' {
  \global
  \repeat volta 3 {
    % Intro
    <ef bf g>8 ef <g bf df> bf <g bf c> bf <g bf> ef
    <af ef c>4 \tuplet 3/2 { <af ef c>8 <af ef c> <af ef c> } <af ef c>8 ef c ef
    % Verse
    <c' af ef>8 af ef af <c af ef> af ef af 
    <df af f> af f af <df af f> af f af
    <df af f> af f bf <df af f> af f af
    <ef' bf g> bf g bf <g bf ef> bf ef bf 
    <df bf f> bf f bf <df af f> af f bf
    <c g> g ef g <bf g ef> g ef g
    <bf g ef> g ef af <g ef bf> ef d bf
    <ef bf g> ef g bf g bf g ef
    <c' af ef> af ef af <c af ef> af ef af 
    <df af f> af f af <df af f> af f af
    <df af f> af f bf <df af f> af f af
    <ef' bf g> bf g bf <g bf ef> bf ef bf
    <df bf f> bf f bf <df bf f> bf f c'
    <df bf f> bf f bf <ef, g bf> g bf g
    <af f d> f g bf <c af ef> ef, <bf' g ef> df,
    <af' ef c> ef c ef af[ ef] af c
    % Chorus
    <c af ef>4. <c af ef>16 <c af ef> <c af ef>4. <c af ef>16 <c af ef>
    <c af ef>4. <c af ef>16 <c af ef> <c af ef>8 <c af ef> <c gf ef> <c gf ef>
    <af f df>4. <bf f df>16 <bf f df> <af f df>8 <af f df> <af f df> <af f df>
    <af ef c>4 \tuplet 3/2 { <af ef c>8 <af ef c> <af ef c> } <af ef c>8 <af ef c> <af ef c> <af ef c>
    <bf g ef>4. <bf g ef>16 <bf g ef> <bf g ef>8 <bf g ef> <c af ef> <c af ef>
    <df bf g>4 \tuplet 3/2 { <bf g ef>8 <bf g ef> <bf g ef> } <bf g ef>8 <bf g ef> <bf g ef> <bf g ef>
    <ef bf g>4 <f bf, g> <ef bf g> <df bf g>
    <c af ef>4 \tuplet 3/2 { <c af ef>8 <c af ef> <c af ef> } <c af ef>8 <c af ef> <df g, ef> <df g, ef>
    <c af ef>4. <c af ef>16 <c af ef> <c af ef>4. <c af ef>16 <c af ef>
    <c af ef>4. <c af ef>16 <c af ef> <c af ef>8 <c af ef> <c gf ef> <c gf ef>
    <df af f>4. <df af f>16 <df af f> <df bf f>8 <df bf f> <c g ef> <c g ef>
    <df bf f >4 \tuplet 3/2 { <df bf f>8 <df bf f> <df bf f> } <df bf f>8 <df bf f> <df bf g> <df bf g>
    <c af ef>4. <c af ef>16 <c af ef> <c af ef>4. <c af ef>16 <c af ef>
    <c af ef>4. <c af ef>16 <c af ef> <c af ef>8 <c af ef> <af ef> <af ef>
  }
  \alternative {
    {
      <bf g ef>8 <bf g ef>16 <bf g ef> <bf g ef>8 <bf g ef> <bf g ef> <bf g ef> <bf g df> <bf g df>
      <af ef c>4 \tuplet 3/2 { <af ef c>8 <af ef c> <af ef c> } <af ef c>4 <af ef c>
      <c af f>4 <c af f> <c af f> <df bf f>
      <ef c af>4 <c af ef> <bf g ef> <af f df>
    } {
      <ef' bf g>8 <ef bf g>16 <ef bf g> <ef bf g>8 <ef bf g> <ef bf g> <ef bf g> <ef df bf g> <ef df bf g>
    }
  }
  <af ef c>4 \tuplet 3/2 { <af ef c>8 <af ef c> <af ef c> } <af ef c>4 r
}

left = \relative c, {
  \global
  \repeat volta 3 {
    % Intro
    ef4 ef f g
    af4 \tuplet 3/2 { af8 af af } af4 ef
    % Verse
    af4 af bf c
    df2 df4 c
    df2 bf2
    ef,2 ef4 ef'
    df2 df4 bf
    ef4 ef df c
    bf af g f
    ef df' c bf
    af2 bf4 c
    df2 df4 c
    df2 bf2
    ef,2 ef4 ef'
    df2 df4 c
    bf2 g
    bf2 ef,
    af4 ef f g
    % Chorus
    af4 af af ef
    af af af c,
    df df f g
    af af ef af
    ef bf' g ef
    bf' ef, g bf
    ef, ef f g
    af ef f g
    af af af ef
    af af af c
    df df df c
    bf bf ef ef
    af, af af ef
    af af af c,8 df
  }
  \alternative {
    {
      ef4 ef f g
      af4 \tuplet 3/2 {af8 af af} af4 af
      df4. df8 c4 bf
      af4. af8 c,4 df
    } {
      ef4 ef f g
    }
  }
  af4 \tuplet 3/2 {af8 af af} af4 r4
}

sopranoVoicePart = \new Staff \with {
  instrumentName = "Solo"
  midiInstrument = "choir aahs"
} <<
  \new Voice = "control" { \oneVoice \control }
  \new Voice = "soprano" { \oneVoice \sopranoVoice }
>>

pianoPart = \new PianoStaff \with {
  instrumentName = "Pno."
} <<
  \new Staff = "right" \with {
    midiInstrument = "acoustic grand"
  } \right
  \new Staff = "left" \with {
    midiInstrument = "acoustic grand"
  } { \clef bass \left }
>>

\score {
  <<
    \sopranoVoicePart
    \new Lyrics \lyricsto "soprano" {
      \verseOne \chorus \chorusLastLineOne \chorusLastLineTwo 
    }
    \new Lyrics \lyricsto "soprano" \verseTwo
    \new Lyrics \lyricsto "soprano" \verseThree
    \pianoPart
  >>
  \layout {
    \context {
      \Score
      \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/8)
    }
    \context {
      \Lyrics
      \override LyricSpace.minimum-distance = #0.7
    }
  }
}

\score {
  \unfoldRepeats \articulate
  <<
    \sopranoVoicePart
    \new Lyrics \lyricsto "soprano" {
      \verseOne \chorus \chorusLastLineOne
      \verseTwo \chorus \chorusLastLineOne
      \verseThree\chorus \chorusLastLineTwo
    }
    \pianoPart
  >>
  \midi {  }
}
